#!/usr/bin/env bash
tamano1=100
tamano2=1000
tamano3=2000
case "$1" in
  orig1)
  build/./p5 mandelbrot.ppm $tamano1
  ;;
  orig2)
  build/./p5 mandelbrot.ppm $tamano2
  ;;
  orig3)
  build/./p5 mandelbrot.ppm $tamano3
  ;;
  omp1)
  build/./p5_omp mandelbrot_omp.ppm $tamano1
  ;;
  omp2)
  build/./p5_omp mandelbrot_omp.ppm $tamano2
  ;;
  omp3)
  build/./p5_omp mandelbrot_omp.ppm $tamano3
  ;;
  runtime1)
  build/./p5_runtime mandelbrot_rt.ppm $tamano1
  ;;
  runtime2)
  build/./p5_runtime mandelbrot_rt.ppm $tamano2
  ;;
  runtime3)
  build/./p5_runtime mandelbrot_rt.ppm $tamano3
  ;;
  seq1)
  build/./p5_seq mandelbrot_seq.ppm $tamano1
  ;;
  seq2)
  build/./p5_seq mandelbrot_seq.ppm $tamano2
  ;;
  seq3)
  build/./p5_seq mandelbrot_seq.ppm $tamano3
  ;;
  priv1)
  build/./p5_priv mandelbrot_pri.ppm $tamano1
  ;;
  priv2)
  build/./p5_priv mandelbrot_pri.ppm $tamano2
  ;;
  priv3)
  build/./p5_priv mandelbrot_pri.ppm $tamano3
  ;;
esac
