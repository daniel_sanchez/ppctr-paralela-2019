# P5:  OpenMP Mandelbrot

Daniel Sánchez Díez

20/12/18

## Prefacio

Esta ultima practica pone a prueba conocimientos sobre la programacion paralela como repasar le paralelizacion de funciones y como novedad en este caso tenemos que implementar una seccion crítica de cuatro formas diferentes.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema

- Ejecutado sobre el sistema operativo Ubuntu 18.04.2 LTS 64bits
- Host (`lscpu, cpufreq, lshw, htop`):
  - CPU Intel(R) Core(TM) i5-8250U CPU @ 1.60GHz
  - CPU 8 cores: 1 socket, 2 threads/core, 4 cores/socket
  -CPU flags: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp md_clear flush_l1d
  - CPU @3,4 GHz
  - CPU: L2: 256K, L3: 6144K
  - RAM: 7,7 GiB

## 2. Diseño e Implementación del Software

Comenzamos con el ejercicio 2 de la práctica, que nos pide que paralelicemos un bucle for, lo cual es una tarea que ya hemos hecho con anterioridad: colocamos dicha estructura en una region paralela y establecemos las variables x_max, y_max, x_min e y_min como compartidas, e i, j, n y count_max como privadas. Una vez tenemos eso solo hay que usar la directiva for de OMP y programarlo como dinámico para que se ejecute sin errores.

El ejercicio 3 pide crear secciones críticas a partir de un bloque de código usando las directivas de OpenMP . Lo primero que se ha hecho ha sido encapsular el código original de forma que se ejecute conforme a la compilacion condicional que se haya elegido (lo usaremos para comparar tiempos de ejecucion). Cada una de las variaciones de la seccion crítica está encapsulada en su propia region de compilacion condicional, y así solamente se ejecutará la versión que nosotros queramos.

La implementación de dichas regiones críticas se realizan de la siguiente forma:

- Version de OpenMP: Creamos una region paralela alrededor del bucle y establecemos i y j como variables privadas, n como compartida y hacemos que el número de threads usados sea el maximo. Una vez esto colocamos la diractiva for para paralelizar el bucle y usaremos critical para crear la zona crítica como tal, en la que protegeremos la variable c_max.

- Version de funciones del runtime: Básicamente haremos uso de las funciones del runtime para administrar un lock para la parte crítica. Primero creamos e inicializamos el lock y despues hacemos lo mismo que en el apartado anterior, solo que en vez de usar la directiva critical simplemente se establece el lock al empezar el if y se suelta cuando acabe. al final el lock es destruido.

- Version secuencial: No tiene más misterio que inicializar la zona paralela como anteriormente pero en vez de paralelizar el for, haremos uso de la directiva single para que el primer hilo que llegue hasta el bucle sea el encargado de ejecutarlo, lo que equivale a una ejecucion secuencial.

-Version de variables privadas: Haremos lo mismo que en la version de OpenMP, pero en vez de establecer una región crítica con la directiva critical haremos que al declarar la region paralela se incluya la clausula reduction con la variable c_max.

El resto del cógigo lo dejamos tal cual está, ya que optimizarlo no va a resultar en ninguna mejora sustancial en el tiempo de ejecución.


## 3. Metodología y desarrollo de las pruebas realizadas

Utilizando el benchmark proporcionado, ponemos a prueba la eficiencia del programa.

El programa que se ejecuta desde la terminal es el bench.sh, en cuyo código se llamará al benchsuite.sh, que es el que se encarga de llamar a nuestro programa directamente. Lo que hace bench.sh es ejecutar diferentes variaciones del programa, con diferentes secciones críticas. A su vez,dentro de cada caso ejecutamos benchsuite.sh 10 veces para comparar resultados, y calculando la media evitamos en la medida de lo posible las fluctuaciones en los tiempos.

Si nos centramos en benchsuite.sh veremos que tiene 6 casos de ejecucion, que van alternando diferentes parámetros. Tres de estos casos son la version secuancial, cada una de ellas con tamaños de buffer de 1, 10 y 50, y las otras 3 hacen lo mismo pero con omp.

Los resultados de aplicar el benchmark se imprimen en un archivo de texto llamado resultados.csv, pero los he organizado y modificado en el fichero calculos.ods para calcular los speedups.

Dichos speedups están recopilados en esta tabla hecha en calc:

![](images/speedups.png)

![](images/grafica.png)


En el eje X de la gráfica los números del 1 al 4 representan las cuatro formas de ejecutar la región crítica en este orden: OMP, funciones del runtime, ejecucion secuencial y ejecucion con variables privadas.
Como se puede ver en el primer caso, el speedup es mínimo, mientras que en el caso del runtime y la secuencial se puede ver cómo éste aumenta ligeramente. Sin embargo el contraste aparece cuando nos fijamos en el caso de las variables privadas, porque pasamos de tener speedups por debajo de 1 a superar los 5 puntos.

## 4. Discusión

Con respecto a la primera parte de la práctica, tenemos que hacer un analisis de cuello de botella para ver que zonas del código producen más carga en la cpu. Para ello ejecutaremos el programa con el comando:

```sh
valgrind --tool=callgrind --dump-instr=yes ./p5
```

Una vez se genera el archivo lo abrimos y con la interfaz podemos seleccionar que funciones y secciones del código cargan con el mayor peso de la ejecución. En el caso de esta práctica veremos esto:

![](images/captura.png)

En un principio puede parecer que el problema radica en que la función explode ejecuta muchas instrucciones pero en realidad todo el peso viene porque se la llama un número muy elevado de veces.

Con respecto al test de cobertura para ver si hay líneas que no se ejecutan.

Usaremos los comandos:

```sh
gcc -fprofile-arcs -ftest-coverage -g -o build/p5test src/p5.c -lm

build/./p5test

gcov p5.gcno

```

Abrimos el archivo que se genera y lo analizamos.

![](images/cobertura.png)

Como se puede observar en la captura, al lado izquierdo del código se puede observar el número de veces que se ejecuta dicha línea. En el caso de ver varias almohadillas, significa que esa línea nunca se ejecuta.
