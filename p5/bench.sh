#!/usr/bin/env bash
make build

file=resultados.csv

if [ -f "$file" ]
then
  rm $file
fi

touch $file
for season in 1; do
  for benchcase in orig1 orig2 orig3 omp1 omp2 omp3 runtime1 runtime2 runtime3 seq1 seq2 seq3 priv1 priv2 priv3; do
    echo $benchcase
    #echo $benchcase >> $file
    for i in `seq 1 1 10`;
    do
      printf "$i:\n" # iteration
      ./benchsuite.sh $benchcase >> $file # results dumped
      sleep 1
    done
  done
done
