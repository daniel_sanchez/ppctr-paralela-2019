# P4:  VideoTask

Daniel Sánchez Díez

19/12/18

## Prefacio

Esta práctica sirve para poner a prueba la capacidad de la diractiva task de OpenMP, y nos ayuda a entender cómo funciona de forma más clara.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema

- Ejecutado sobre el sistema operativo Ubuntu 18.04.2 LTS 64bits
- Host (`lscpu, cpufreq, lshw, htop`):
  - CPU Intel(R) Core(TM) i5-8250U CPU @ 1.60GHz
  - CPU 8 cores: 1 socket, 2 threads/core, 4 cores/socket
  -CPU flags: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp md_clear flush_l1d
  - CPU @3,4 GHz
  - CPU: L2: 256K, L3: 6144K
  - RAM: 7,7 GiB

## 2. Diseño e Implementación del Software

Lo primero que se ha hecho ha sido encapsular el código original de forma que solo se ejecuta en compilacion condicional, y por otro lado la parte que vamos a optimizar con OpenMP quedará envuelta de la misma manera. Así aolo se ejecutará la versión que nosotros queramos.

La implementación de dicha paralelización la llevaremos a cabo en el bucle while, ya que no sabemos con exactitud cuantas iteraciones va a ejecutar, además de que vemos que la carga de trabajo es mucho mayor en esa zona comparado con el resto. La razón por la que sabemos que el número de iteraciones va a ser variable es porque el bucle recorre el archivo que se le pasa hasta que encuentra el final, pero no existe ningún numero de iteraciones conocido.
Para llevar esto a cabo abriremos una region paralela con OpenMP que abarque el bucle e inmediatamente después abrimos otra región master, lo que quiere decir que esa parte del código solo la ejecuta el hilo principal aunque sea una region paralelizada. Una vez estamos dentro del bucle crearemos una region con la directiva task que ejecute el if y la llamada a la funcion gauss, aunque la funcion fwrite la quitaremos de aquí por una razón concreta, que es la necesidad de escribir los frames en orden, ya que puede que se acabe de filtrar uno antes que el anterior. Despues de esto se cierra la region de la task y aumentamos un contador, que llevará la cuenta de los frames del buffer que están ocupados. Si dicha cuenta es igual al tamaño del buffer entonces es cuando se abrirá un bucle que llamará a fwrite y con ella escribirá definitivamente los frames. Además volvemos a poner a 0 el contador, ya que el buffer va a quedar vacío. Aquí es donde acaba el bucle while, y con él se cierra la region del master y la paralela, pero aún no hemos acabado. El buffer puede que no se haya llenado pero aún así tiene algún frame, así que ejecutaremos otro bucle  que llame a fwrite las veces necesarias para vaciarlo.

El resto del cógigo lo dejamos tal cual está, ya que optimizarlo no va a resultar en ninguna mejora sustancial en el tiempo de ejecución.


## 3. Metodología y desarrollo de las pruebas realizadas

Utilizando el benchmark proporcionado, ponemos a prueba la eficiencia del programa.

El programa que se ejecuta desde la terminal es el bench.sh, en cuyo código se llamará al benchsuite.sh, que es el que se encarga de llamar a nuestro programa directamente. Lo que hace bench.sh es ejecutar diferentes variaciones del programa, con diferentes tamaños de buffer y alternando entre la versión secuencial y paralela. A su vez,dentro de cada caso ejecutamos benchsuite.sh 10 veces para comparar resultados, y calculando la media evitamos en la medida de lo posible las fluctuaciones en los tiempos.

Si nos centramos en benchsuite.sh veremos que tiene 6 casos de ejecucion, que van alternando diferentes parámetros. Tres de estos casos son la version secuancial, cada una de ellas con tamaños de buffer de 1, 10 y 50, y las otras 3 hacen lo mismo pero con omp. Usamos esos tamaños porque el maximo numero de frames que crea generator es 50.

Los resultados de aplicar el benchmark se imprimen en un archivo de texto llamado resultados.csv, pero los he organizado y modificado en el fichero speedups.ods para calcular los speedups.

Dichos speedups están recopilados en esta tabla hecha en calc:

![](images/speedups.png)

![](images/grafica.png)

Se puede observar una diferencia en el tiempo de ejecucion cuando usamos las versiones de OMP frente a la original usando 10 y 50 buffers, lo que quiere decir que las optimizaciones realizadas tienen impacto en la ejecucion, pero sin embargo, el uso de un único buffer nos da un speedup de prácticamente 1. Esto se puede explicar porque el hilo master genera las tareas, y se asigna una a cada frame, lo que quiere decir que cuando se comprueba si el buffer está lleno la condicion siempre se cumple, y el taskwait deja pasar dicho hilo a la funcion de escritura. La diferencia más observable es la que encontramos al pasar de tamaño de buffer 1 a 10, ya que el speedup es varias veces mayor, pero cuanto más se acerca dicho tamaño al número total de frames vemos que el speedup es cada vez más estable. Esto se debe a que todas las tareas llegan al mismo buffer y, por extension, al taskwait a la vez, haciendo que la paralelización sea la maxima.

## 4. Discusión

La mayor complicacion de esta práctica es comprender qué se hace con la llamada a fwrite, ya que hay que tener en cuenta el tamaño del buffer a la hora de comprobar si quedan frames que no se hayan escrito todavía.

Además hay que entender como funciona la directiva task y como y donde aplicarla.
Lo que hace dicha directiva es que el hilo master divide la parte del código incluido en tareas y le asigna una a cada hilo. Además esto tene ventajas porque, al no saber el número de iteraciones que va a ejecutar el bucle, es mejor no dividirlo, sino que nos centramos en la zona interna. Adicionalmente la función que se llama no la optimizamos porque el problema no es ella, sino el número de veces que se llama, así que es mejor paralelizar la llamada como tal.
