#!/usr/bin/env bash
make build
build/./generator
file=resultados.csv

if [ -f "$file" ]
then
  rm $file
fi

touch $file
for season in 1; do
  for benchcase in seq1 seq2 seq3 omp1 omp2 omp3; do
    echo $benchcase
    #echo $benchcase >> $file
    for i in `seq 1 1 5`;
    do
      printf "$i:\n" # iteration
      ./benchsuite.sh $benchcase >> $file # results dumped
      sleep 1
    done
  done
done
