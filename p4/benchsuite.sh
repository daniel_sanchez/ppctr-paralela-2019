#!/usr/bin/env bash
buff1=1
buff2=10
buff3=50
case "$1" in
  seq1)
  build/./seq_video_task $buff1
  ;;
  seq2)
  build/./seq_video_task $buff2
  ;;
  seq3)
  build/./seq_video_task $buff3
  ;;
  omp1)
  build/./omp_video_task $buff1
  ;;
  omp2)
  build/./omp_video_task $buff2
  ;;
  omp3)
  build/./omp_video_task $buff3
  ;;
esac
