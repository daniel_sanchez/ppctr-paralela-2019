#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <thread>
#include <math.h>
#include <condition_variable>
#include <mutex>
#include <sys/time.h>

//#define DEBUG
//#define FEATURE_LOGGER

std::mutex bloqueo;
void funcion(double* lista, int puntero, int long_list, char* op, int iter);
void funcion_logger(std::condition_variable *cv_log);
double auxiliar(char* op);

int l_array;
char* operacion;
char* modo;
int n_threads;
double* array;
int l_subArray;
double resul=0;

bool primeroAcabo=false;
struct timeval t1, t2;
double tiempoTranscurrido;

#ifdef FEATURE_LOGGER
void funcion_logger(std::condition_variable *cv_log);
double resul_logger=0;
int contador=0;
std::condition_variable cv;
std::mutex bloqueo2;
bool disponible=false;
bool disponible2=false;
double *puntero_aux;
#endif

int main(int argc, char *argv[]){

  if(argc<4 || argc>=6){
    printf("Error en numero de variables\n");
    exit(-1);
  }

  //Guardamos la longitud del array
  l_array=atoi(argv[1]);

  //Guardamos el tipo de operacion
  operacion=argv[2];

  //Multi o mono
  modo=argv[3];

  //Si la ejecucion es multithread cogemos el numero de hilos, si no es 1 por defecto
  if(argc==5 && strcmp(modo, "--multi-thread")==0){

    n_threads=atoi(argv[4]);

    if(n_threads>12 || n_threads<1){
      #ifdef DEBUG
      printf("Numero de threads no válido\n");
      #endif
      exit(-1);
    }
  }else if(strcmp(modo, "--single-thread")==0){
    n_threads=1;
  }else{
    printf("Modo mal especificado\n");
    exit(-1);
  }

  //Declaramos el array en el heap y lo llenamos
  array = (double*) malloc(sizeof(double)*l_array);

  for(auto i=0; i<l_array;i++){
    array[i]=i+1;
    #ifdef DEBUG
    printf("%f\n", array[i]);
    #endif
  }

  //A partir de aqui empieza la ROI
  gettimeofday(&t1, NULL);

  #ifdef FEATURE_LOGGER
  std::condition_variable cv_log;
  std::thread log;
  log = std::thread(funcion_logger, &cv_log);
  double array_logger[n_threads];
  puntero_aux = array_logger;
  #endif

  l_subArray=l_array/n_threads;
  std::thread threads[n_threads];

  #ifdef DEBUG
  printf("Threads creados\n");
  printf("l_subarray= %d\n", l_subArray);
  printf("Resto= %d\n", l_array-(l_subArray*n_threads));
  #endif

  //Inicializamos los hilos con la funcion que ejecutan
  for (auto i=0; i<n_threads; i++){
    threads[i] = std::thread(funcion, array, l_subArray*i, l_subArray, operacion, i);
  }

  //Una vez los hilos acaban juntamos los resultados
  for (auto i=0; i<n_threads; i++){
    threads[i].join();
  }

  #ifdef DEBUG
  printf("Resultado = %f\n", resul);
  #endif

  //TODO
  #ifdef FEATURE_LOGGER
  std::unique_lock<std::mutex> bloq_unico(bloqueo);
  cv_log.wait(bloq_unico, []{return disponible2;});
  bloq_unico.unlock();
  log.join();

  if(resul==resul_logger){
    printf("Concuerdan los resultados del logger y el main. logger: %f, threads: %f\n", resul_logger, resul);
  }else{
    printf("Los resiltados del logger: %f, y del main: %f no coinciden\n", resul_logger, resul);
  }
  #endif

  //Aquí dejamos de medir el tiempo de ejecucion
  gettimeofday(&t2, NULL);

  //El tiempo que se ha tardado es la diferencia entre el final y el inicial
  tiempoTranscurrido = (t2.tv_sec - t1.tv_sec) * 1000.0;
  tiempoTranscurrido += (t2.tv_usec - t1.tv_usec) / 1000.0;

  printf("Tiempo de la ejecucion con tamaño %d y %d hilos: %f\n",l_array, n_threads, tiempoTranscurrido);
}

//Funcion que realiza los calculos para la parte paralela
void funcion(double* lista, int puntero, int long_list, char* op, int iter){

  #ifdef DEBUG
  for(auto i=0; i<long_list; i++){
    printf("%f\n", lista[i]);
  }
  #endif

  double aux=0;

  if(strcmp(op,"sum")==0){

    #ifdef DEBUG
    printf("longitud = %d puntero = %d\n",long_list, puntero);
    #endif

    for(auto i=puntero; i<(puntero+long_list); i++){
      aux+=lista[i];

      #ifdef DEBUG
      printf("aux = %f, lista[i]= %f\n", aux, lista[i]);
      #endif
    }

    if(l_array>l_subArray*n_threads && !primeroAcabo){
      primeroAcabo=true;
      funcion(array, l_subArray*n_threads, l_array-(l_subArray*n_threads), op, iter);
    }

    std::lock_guard<std::mutex> lock(bloqueo);{
      resul+=aux;
    }

  }else if(strcmp(op,"sub")==0){

    double aux=0;

    for(auto i=puntero; i<puntero+long_list; i++){
      aux-=lista[i];
    }

    if(l_array>l_subArray*n_threads && !primeroAcabo){
      primeroAcabo=true;
      funcion(array, l_subArray*n_threads, l_array-(l_subArray*n_threads), op, iter);
    }

    std::lock_guard<std::mutex> lock(bloqueo);{
      resul+=aux;
    }

  }else if(strcmp(op,"xor")==0){

    int aux=0;

    for(auto i=puntero; i<puntero+long_list; i++){
      aux = aux^(int)lista[i];
    }

    if(l_array>l_subArray*n_threads && !primeroAcabo){
      primeroAcabo=true;
      funcion(array, l_subArray*n_threads, l_array-(l_subArray*n_threads), op, iter);
    }

    std::lock_guard<std::mutex> lock(bloqueo);{
      resul=(int)resul^aux;
    }
  }
  //Guardamos el resultado del hilo en el array del logger y aumentamos la cuanta de hilos acabados
  #ifdef FEATURE_LOGGER
  puntero_aux[iter] = aux;
  contador++;
  if(!disponible) disponible=true;
  cv.notify_one();
  #endif
}

//Funcion que ejecuta el hilo logger cuando se le llama
#ifdef FEATURE_LOGGER
void funcion_logger(std::condition_variable *cv_log){

  double log_arr[n_threads];

  //Bucle que espera a que todos los hilos hayan acabado
  while(contador != n_threads){
    std::unique_lock<std::mutex> l(bloqueo2);
    cv.wait(l, []{return disponible;});
    disponible = false;
    l.unlock();
  }

  for(auto i=0 ; i<n_threads ; i++){
    log_arr[i] = puntero_aux[i];
  }

  #ifdef DEBUG
  printf("Seccion del logger\n");

  for(auto i=0;i<n_threads;i++){
    printf("Thread %d log:%f.\n", i, log_arr[i]);
  }
  #endif
  //Juntamos los valores del array del logger
  double aux=0;
  if(strcmp(operacion, "sum")==0){
    aux += auxiliar(operacion);
  } else if(strcmp(operacion, "sub")==0) {
    aux += auxiliar(operacion);
  } else if(strcmp(operacion, "xor")==0) {
    aux = (int)aux ^(int) auxiliar(operacion);
  }
  printf("Resultado del log: %f\n", aux);

  std::lock_guard<std::mutex> guard(bloqueo);
  cv_log->notify_one();
  resul_logger=aux;
  disponible2=true;
}
#endif

//Funcion auxiliar que opera sobre un array de forma secuencial
double auxiliar(char* op){
  double aux=0;
  if(strcmp(op,"sum")==0){
    for(auto i=0; i<l_array;i++){
      aux+=array[i];
    }
  }else if(strcmp(op,"sub")==0){
    for(auto i=0; i<l_array;i++){
      aux-=array[i];
    }
  }else if(strcmp(op,"xor")==0){
    for(auto i=0; i<l_array;i++){
      aux=(int)aux ^ (int)array[i];
    }
  }
  return aux;
}
