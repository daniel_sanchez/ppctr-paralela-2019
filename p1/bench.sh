#!/usr/bin/env bash
make build
file=resultados.log

if [ -f "$file" ]
then
  rm $file
fi

touch $file
for season in 1 2; do
  for benchcase in st1 st2 mt1 mt2; do
    echo $benchcase >> $file
    for i in `seq 1 1 10`;
    do
      printf "$i:" >> $file # iteration
      ./benchsuite.sh $benchcase >> $file # results dumped
      sleep 1
    done
  done
done
