# P1: Multithreading en C++

Daniel Sánchez Díez

27/11/18

## Prefacio

Durante esta práctica, que ha supuesto un salto considerable de dificultad en el nivel de entendmiento de
la programación que poseía anteriormente, he aprendido a paralelizar y optimizar operaciones que
trabajan con arrays de gran tamaño gracias al uso de hilos.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema

- Ejecutado sobre el sistema operativo Ubuntu 18.04.2 LTS 64bits
- Host (`lscpu, cpufreq, lshw, htop`):
  - CPU Intel(R) Core(TM) i5-8250U CPU @ 1.60GHz
  - CPU 8 cores: 1 socket, 2 threads/core, 4 cores/socket
  -CPU flags: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp md_clear flush_l1d
  - CPU @3,4 GHz
  - CPU: L2: 256K, L3: 6144K
  - RAM: 7,7 GiB

## 2. Diseño e Implementación del Software

la práctica se ha desarrollado únicamente en el fichero p1.cpp, en el que encontramos varios bloques de código.

El primero de ellos es la cabecera, que contiene todas las librerías, constantes y declaraciones de variables globales
que se van a usar a lo largo de toda la ejecucion, ademas de declarar todo prototipo de función que se necesite
para que funcione todo correctamente. También declaramos el mutex que se usará a la hora de modificar cualquier variable
global desde los procesos paralelos.

Lo siguiente que encontramos es el main, que lo primero que hace es comprobar que el número de parámetros no exceda el
límite, en cuyo caso lanza un mensaje y termina la ejecución. Lo siguiente que hace es coger dichos parámetros y
los guarda en las variables globales correspondientes, que son la longitud del array con el que se va a trabajar,
la operacion a realizar (suma, resta o XOR), el modo de ejecucion (secuencial o paralela) y el número de hilos.
Tras esto se reserva en memoria dinámica el espacio necesario para almacenar el array usando malloc,
y se rellena dicha memoria con el contenido del mismo.

```c++
array = (double*) malloc(sizeof(double)*l_array);
```

Ahora comprobaremos si el programa se va a ejecutar de forma secuencial o paralela. Para ello tenemos un
if que comprueba el modo de ejecucion: si es multi thread cogerá el número de hilos de los parámetros,
y en caso contrario pondrá el número de hilos a 1, que equivale a una ejecucion secuencial.
A continuacion se declara un array de hilos, que inicializaremos después, y se calcula la longitud del
subarray que se origina al dividir el array completo entre cada uno de ellos. En el caso de la ejecucion
secuencial evidentemente no habrá problemas, pero si no es así puede que haya algún sobrante
que no pertenezca a ningún hilo. Hay soluciones explicadas más adelante para sortear este obstáculo.
Posteriormente tenemos un bucle que, usando la función que ejecutan los hilos, que se explica más adelante,
se reparte la tarea correspondiente a cada uno de ellos. Lo siguiente es otro bucle que
combina los resultados de cada hilo usando "join()" y se acaba por imprimir el resultado por pantalla.

Lo siguiente que tenemos es la función que opera con el array, que recibe como parámetros
el array, un puntero indicando en que parte del mismo se encuentra e segmento a operar, la longitud del segmento
y por último la operacion. Al igual que antes comenzamos con los ifs para distinguir la operacion, y tras esto
declaramos una variable auxiliar para guardar los resultados del segmento concreto, y luego usando el mutex
actualizamos la variable global "resul". Un apunte a tener en cuenta es que la division del array puede generar un resto
que no le corresponde a ningún hilo, como se vio antes, en cuyo caso se ha implementado un sistema mediante el
cual el hilo que primero acabe su tarea se encargará de esa parte. Para ello hay una variable global booleana
inicializada a false que cambia de valor cuando el primer hilo que acabe llegue a ella. Gracias a este valor
booelano habrá un bloque de código que llama a la función de nuevo con el resto sólamente la primera vez que se llega.

```c++
if(l_array>l_subArray*n_threads && !primeroAcabo){
  primeroAcabo=true;
  funcion(array, l_subArray*n_threads, l_array-(l_subArray*n_threads), op, iter);
}
```

Fuera de la parte básica nos encontramos con el hilo logger, que sólo se ejecuta si tenemos definida la variable
FEATURE_LOGGER. Dicho hilo se encarga de ir recogiendo los resultados de las ejecuciones de cada hilo normal y
posteriormente comprueba si el programa ha calculado bien el resultado, para lo que compara la solucion que ha
reunido de los hiloscon la que calcula el código principal. Para llevar a cabo esto lo primero que se hace es
crear una variable condicional y el hilo logger al que le asignamos la funcion que habrá de llamar cuando
sea necesario. Adicionalmente creamos un arrray para guardar los resultados de los hilos y un puntero
auxiliar que apunte al inicio del mismo.
Despues de que cada hilo devuelva su resultado, el logger tomará el mutex y se pondrá a esperar hasta que tenga
todos los resultados, en cuyo caso se desbloquea. Tras esto se comprueba que los resultados del main y
el logger coinciden y se notifica.

La funcion del logger se encarga de esperar a que los hilos acaben, copia el array del logger y calcula el
resultado final de forma secuencial por su cuenta.


## 3. Metodología y desarrollo de las pruebas realizadas

Utilizando el benchmark proporcionado, ponemos a prueba la eficiencia del programa.

El programa que se ejecuta desde la terminal es el bench.sh, en cuyo código se llamará al benchsuite.sh, que es el que se
encarga de llamar a nuestro programa directamente.
Lo que hace bench.sh es ejecutar dos bucles o "estaciones", que repiten el mismo codigo pero lo usamos para ver
si el tiempo de ejecucion de nuestro programa es consistente. Dentro de cada estacion habrá diferentes variaciones
en la ejecucion, con diferentes tamaños y cantidad de hilos, que se corresponden con las siglas st1 (single thread 1),
st2, mt1 (multi thread 1) y mt2. A su vez,dentro de cada caso ejecutamos benchsuite.sh 10 veces para comparar resultados.

Si nos centramos en benchsuite.sh veremos que hace 4 llamadas diferentes de nuestro programa
dependiendo del caso en el que estemos.
Por ejemplo, el caso st1 ejecuta el programa con un array de tamaño 10240 y un hilo (ejecucion secuencial) y mt2 lo hará
con 10240000 y 8 hilos.

Los resultados de aplicar el benchmark se imprimen en un archivo de texto llamado resultados.log, en el que se incluye
simplemente por limpieza el tamaño del array, número de hilos y tiempo de ejecucion.

Dichos resultados están recopilados en este documento de calc:

![](images/tiempos.png)

En el caso de las ejecuciones con 10240 elementos los resultados generan una grafica tal que esta:

![](images/grafica1.png)

Como podemos observar, en el caso de usar arrays pequeños en los que el tiempo de ejecucion es muy pequeño, resulta mas
eficiente utilizar la ejecucion secuencial que la paralela.

Sin embargo, en cuanto el tiempo de ejecucion comienza a aumentar veremos que sí que resulta más eficiente paralelizar:

![](images/grafica2.png)

Se puede observar una diferencia sustancial en el tiempo de ejecucion cuando usamos 8 hilos frente a 1.

## 4. Discusión

Esta práctica ha supuesto un desafío no solo porque se trate de programación paralela, sino también porque
se aplica el uso de mutexes y hay que razonar adecuadamente el reparto de las tareas entre hilos.

Personalmente la parte que más problemas ha causado es el hilo logger, porque hay que comprender el
concepto y utilizacion de las variables condicionales, que son elementos de la programación
que no habíamos visto hasta ahora y resultan confusos a la hora de entenderlos por primera vez.

Con respecto a la pregunta planteada, a continuación explico cómo he hecho las mediciones de tiempo de
ejecucion.
Para calcular el intervalo de tiempo que se tarda en ejecutar la region de interés (ROI) haremos uso de la
API gettimeofday. Para poder usarla necesitamos dos structs de timeval, que llamaremos t1 y t2, que sirven
para almacenar tiempos individuales. De esta forma, si llamamos a gettimeofday en un punto del programa y
después lo volvemos a hacer en un punto posterior podemos restar dichas mediciones, y la diferencia resultante
es el intervalo que nos interesa.
En mi caso la región que he analizado comienza justo al inicio de la parte en la que se va a notar diferencia
en el caso de ejecutar la version secuencial o paralela, que es antes de declarar el array de hilos, y finaliza
una vez hacemos el join() de los mismos.

La forma de calcular el resultado es la siguiente:

```c++
tiempoTranscurrido = (t2.tv_sec - t1.tv_sec) * 1000.0;
tiempoTranscurrido += (t2.tv_usec - t1.tv_usec) / 1000.0;
```
donde tiempoTranscurrido es una variable global declarada al inicio del código.
