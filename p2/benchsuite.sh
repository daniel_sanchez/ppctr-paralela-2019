#!/usr/bin/env bash
size1=50000000
size2=100000000
nthreads1=1
nthreads2=2
nthreads3=4
nthreads4=8
case "$1" in
  seq1)
  build/./cpp_seq $size1
  ;;
  seq2)
  build/./cpp_seq $size2
  ;;
  par1)
  build/./cpp_par $size1 $nthreads1
  ;;
  par2)
  build/./cpp_par $size1 $nthreads2
  ;;
  par3)
  build/./cpp_par $size1 $nthreads3
  ;;
  par4)
  build/./cpp_par $size1 $nthreads4
  ;;
  par5)
  build/./cpp_par $size2 $nthreads1
  ;;
  par6)
  build/./cpp_par $size2 $nthreads2
  ;;
  par7)
  build/./cpp_par $size2 $nthreads3
  ;;
  par8)
  build/./cpp_par $size2 $nthreads4
  ;;
  omp1)
  build/./cpp_omp $size1 $nthreads1
  ;;
  omp2)
  build/./cpp_omp $size1 $nthreads2
  ;;
  omp3)
  build/./cpp_omp $size1 $nthreads3
  ;;
  omp4)
  build/./cpp_omp $size1 $nthreads4
  ;;
  omp5)
  build/./cpp_omp $size2 $nthreads1
  ;;
  omp6)
  build/./cpp_omp $size2 $nthreads2
  ;;
  omp7)
  build/./cpp_omp $size2 $nthreads3
  ;;
  omp8)
  build/./cpp_omp $size2 $nthreads4
  ;;
esac
