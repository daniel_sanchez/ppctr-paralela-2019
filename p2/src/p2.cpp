#include <stdio.h>
#include <math.h>
#include <cstdlib>
#include <stdlib.h>
#include <sys/time.h>
#include <iostream>

#ifdef SEQ
#include "p2.hpp"
void sequential(int);
#endif
//#define RARALELO
//#define OMP

struct timeval t1, t2;
double tiempoTranscurrido;

#ifdef PARALELO
#include <thread>
#include <mutex>
void funcion_parallel(long num_its, int num_hilos, double gap);
void reparto(double *resultado, double gap, long nHilos, long nIters, long indice, long resto);
int llegaPrimero=0;
std::mutex bloqueo;
#endif

#ifdef OMP
#include <omp.h>
void funcion_omp(long num_its, int num_hilos, double gap);
#endif

int main(int argc, char *argv[])
{

  long iteraciones=atol(argv[1]);
  int n_threads=1;

  gettimeofday(&t1, NULL);

  #ifdef SEQ
  Data data = parse(iteraciones);
  sequential(data);
  #endif

  #ifdef PARALELO
  if(argc==3){
    n_threads=atoi(argv[2]);
  }else{
    n_threads=atoi(getenv("OMP_NUM_THREADS"));
  }

  double gap=1.0/iteraciones;
  funcion_parallel(iteraciones, n_threads, gap);
  #endif

  #ifdef OMP
  if(argc==3){
    n_threads=atoi(argv[2]);
  }else{
    n_threads=atoi(getenv("OMP_NUM_THREADS"));
  }

  double gap=1.0/iteraciones;
  funcion_omp(iteraciones, n_threads, gap);
  #endif

  gettimeofday(&t2, NULL);

  tiempoTranscurrido = (t2.tv_sec - t1.tv_sec) * 1000.0;
  tiempoTranscurrido += (t2.tv_usec - t1.tv_usec) / 1000.0;

  printf("%f\n",tiempoTranscurrido);

}

#ifdef PARALELO
void funcion_parallel(long num_its, int num_hilos, double gap){

  std::thread threads[num_hilos];
  double ellipse=0;
  long indice[num_hilos];
  long resto=num_its%num_hilos;

  if(resto!=0){
    llegaPrimero=1;
  }

  for (auto i=0; i<num_hilos; i++){
    indice[i]=(num_its/num_hilos)*i;
    threads[i] = std::thread(reparto, &ellipse, gap, num_hilos, num_its, indice[i], resto);
  }

  for (auto i=0; i<num_hilos; i++){
    threads[i].join();
  }

  ellipse=ellipse*gap;
  ellipse=2.0 * ellipse/0.489897949;
	//printf("ellipse (Paralelo): %f \n", ellipse);
}

void reparto(double *resultado, double gap, long nHilos, long nIters, long indice, long resto){
  double powGap= gap*gap;
  double tmp=0;
  for(auto i=indice;i<nIters/nHilos+indice;i++){
    tmp = tmp + 4/(1+((i+0.5)*(i+0.5)*powGap));
  }
  if(llegaPrimero!=0){
    llegaPrimero=0;
    for(auto i=nIters-resto;i<nIters;i++){
      tmp = tmp + 4/(1+((i+0.5)*(i+0.5)*powGap));
    }
  }
  std::lock_guard<std::mutex>lock(bloqueo);
  *resultado=*resultado+tmp;
}
#endif

#ifdef OMP
void funcion_omp(long num_its, int num_hilos, double gap){
  double powGap= gap*gap;
  double tmp=0;
  long i;
  #pragma omp parallel num_threads(num_hilos) private(i) reduction(+:tmp) shared(powGap,num_its)
  {
    int aux=omp_get_thread_num();
    for(i=i+aux; i<num_its; i=i+num_hilos){
      tmp=tmp+4/(1+(i+0.5)*(i+0.5)*powGap);
    }
  }
  tmp=tmp*gap;
  tmp=2*tmp/0.489897949;
  //printf("ellipse (OpenMP): %f \n",tmp);
}
#endif

#ifdef SEQ
int attach(Data* data, long iterations){
  if (iterations <= 0){
    return -1;
  }
  Aggregator* agg = (Aggregator*)malloc(sizeof(Aggregator));
  agg->separator = (double)1/(double)iterations;
  agg->A = 1.5;
  agg->B = 2.4;
  agg->C = 1.0;
  data->agg = agg;
  return 0;
}

Data parse(long iters){
  // parse 'long' and set numIts
  long numIts = iters;
  Data d;
  d.numIts = numIts;
  d.gap = 1.0/(double)d.numIts;
  d.value = 0.0;
  if (attach(&d, numIts) != 0){
    fprintf(stderr, "error: invalid iterations\n");
    exit(-1);
  };
  return d;
}

void sequential(Data data){
	int i;
  int jump = 2.4;
	double value;
  double tmp = 0;
  double ellipse;

  int id;
  double x;
  for (i=0; i<data.numIts; i=i+1) {
    x = (i+(double)1/(int)2.0) * data.gap;
    tmp = tmp + pow(2,2)/(1+pow(x, 2));
  }
  value = tmp * data.agg->separator;
  ellipse = 2 * value / sqrt((4 * data.agg->A * data.agg->C - pow(data.agg->B, 2)));

	//printf("ellipse: %f \n", ellipse);
}
#endif
