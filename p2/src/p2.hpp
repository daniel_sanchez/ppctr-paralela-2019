#ifndef P2_HPP
#define P2_HPP

struct Aggregator {
  double separator;
  double A;
  double B;
  double C;
};

struct Data {
  double value;
  double gap;
  Aggregator* agg;
  long numIts;
};

void sequential(Data data);
Data parse(long iters);
int attach(Data* data, long iterations);

#endif // P2_HPP
