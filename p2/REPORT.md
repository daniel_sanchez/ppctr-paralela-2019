# P2: Optimizing

Daniel Sánchez Díez

16/12/18

## Prefacio

Esta práctica se ha centrado en repasar los conceptos aprendidos en la primera sobre paralelizacion con hilos a ña vez que aprendemos a manejar conceptos básicos de OpenMP.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema

- Ejecutado sobre el sistema operativo Ubuntu 18.04.2 LTS 64bits
- Host (`lscpu, cpufreq, lshw, htop`):
  - CPU Intel(R) Core(TM) i5-8250U CPU @ 1.60GHz
  - CPU 8 cores: 1 socket, 2 threads/core, 4 cores/socket
  -CPU flags: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp md_clear flush_l1d
  - CPU @3,4 GHz
  - CPU: L2: 256K, L3: 6144K
  - RAM: 7,7 GiB

## 2. Diseño e Implementación del Software

La práctica se ha desarrollado únicamente en el fichero p2.cpp, en el que encontramos varios bloques de código.

El primero de ellos es la cabecera, que contiene todas las librerías que se van a usar a lo largo de toda la ejecucion, ademas de declarar todo prototipo de función que se necesite para que funcione todo correctamente. También declaramos el mutex que se usará a la hora de modificar cualquier variable global desde los procesos paralelos.

Lo primero que haremos es envolver las secciones de codigo que pertenecen a la ejecucion secuencial en ifs condicionales que solo se ejecutarán si así lo deseamos durante la compilación condicional.

```c++
#ifdef SEQ
Data data = parse(iteraciones);
sequential(data);
#endif
```
Posteriormente encontramos una región de código que solo se ejecutará en el caso de tratarse del caso paralelo hecho con threads. Dicho codigo consigue el numero de iteraciones por parámetro, calcula la variable auxiliar "gap" y llama a la funcion que ejecutará la parte paralela. Dicha función, que se llama "funcion_parallel" crea los hilos necesarios y se encarga de repartir las iteraciones del bucle que calcula la elipse entre todos ellos usando la funcion "reparto". Tras eso se recogen los resultados y se imprime por pantalla el resultado total.
La funcion "reparto" a su vez se encarga de hacer la funcionalidad del bucle secuencial pero con una porcion del mismo en vez del completo. Al finalizar la operacion del bucle, gracias al uso de un mutex guardamos el resultado en la variable que se ha pasado como puntero.

Adicionalmente existe otra seccion del código dedicada a paralelizar usando OpenMP, que se ejecuta de la misma forma, es decir, usamos la compilacion condicional para seleccionar esa parte independiente. La parte del main es exactamente igual a la paralelizacion anterior, pero la diferencia llega a la hora de hacer la funcion que trabajará con el array. EN vez de dividir las iteraciones entre los hilos a mano crearemos una region paralela en la que introduciremos el bucle, que tendrá el numero de hilos que recinimos por consola. Tendremos una variable auxiliar con el numero del hilo, que nos valdrá para poder asignar el valor inicial a la variable de control del bucle, y se sumará el numero de hilos a cada iteracion hasta que se supere el número establecido.

## 3. Metodología y desarrollo de las pruebas realizadas

Utilizando el benchmark proporcionado, ponemos a prueba la eficiencia del programa.

El programa que se ejecuta desde la terminal es el bench.sh, en cuyo código se llamará al benchsuite.sh, que es el que se encarga de llamar a nuestro programa directamente. Lo que hace bench.sh es ejecutar diferentes variaciones del programa, con diferentes iteraciones y cantidad de hilos. A su vez,dentro de cada caso ejecutamos benchsuite.sh 10 veces para comparar resultados, y calculando la media evitamos en la medida de lo posible las fluctuaciones en los tiempos.

Si nos centramos en benchsuite.sh veremos que tiene 18 casos de ejecucion, que van alternando diferentes parámetros. Dos de ellos son el secuencial que aplican los dos tamaños de iteracion, otros 8 aplican la ejecucion paralela con ambos tamaños y usando 1, 2, 4 y 8 hilos, y para acabar las 8 ultimas hacen lo mismo pero con omp.

Los resultados de aplicar el benchmark se imprimen en un archivo de texto llamado resultados.csv, pero los he organizado y modificado en el fichero speedups.ods para calcular los speedups.

Dichos speedups están recopilados en esta tabla hecha en calc:

![](images/speedups.png)

Como podemos observar, en el caso de usar 50 millones o 100 millones de iteraciones, vemos que el speedup suele ser prácticamente el mismo en ambos, y a la vez vemos que usar OpenMP genera un código aún mas eficiente que con la paralelización con c++, lo cual se ve incluso mejor cuantos más hilos se usan.

Se puede observar una diferencia sustancial en el tiempo de ejecucion cuando usamos las versiones paralelas frente a la original, incluso cuando ejecutamos la version paralela de un solo hilo (que equivale a una ejecucion secuencial), lo que quiere decir que el código proporcionado podía ser optimizado incluso sin paralelizar.

## 4. Discusión

La mayor complicacion de esta práctica es comprender cómo establecer las condiciones del bucle que opera en la función de OpenMP, ya que hay que tener en cuenta el número de hilos que se ha establecido y cómo usarlos correctamente.

La primera pregunta está respondida en el apartado 3, porque se explica como se ha estructurado el código y lo que ejecuta.

Con respecto a la segunda pregunta planteada, a continuación explico cómo he hecho las mediciones de tiempo de ejecucion.
Para calcular el intervalo de tiempo que se tarda en ejecutar la region de interés (ROI) haremos uso de la API gettimeofday. Para poder usarla necesitamos dos structs de timeval, que llamaremos t1 y t2, que sirven para almacenar tiempos individuales. De esta forma, si llamamos a gettimeofday en un punto del programa y después lo volvemos a hacer en un punto posterior podemos restar dichas mediciones, y la diferencia resultante es el intervalo que nos interesa. En mi caso la región que he analizado comienza justo al inicio de la parte en la que se va a notar diferencia en el caso de ejecutar la version secuencial o paralela, que en realidad es todo el main.

La forma de calcular el resultado es la siguiente:

```c++
tiempoTranscurrido = (t2.tv_sec - t1.tv_sec) * 1000.0;
tiempoTranscurrido += (t2.tv_usec - t1.tv_usec) / 1000.0;
```
donde tiempoTranscurrido es una variable global declarada al inicio del código.
