#!/usr/bin/env bash
make build
file=resultados2.csv

if [ -f "$file" ]
then
  rm $file
fi

touch $file
for season in 1; do
  for benchcase in seq1 seq2 par1 par2 par3 par4 par5 par6 par7 par8 omp1 omp2 omp3 omp4 omp5 omp6 omp7 omp8; do
    echo $benchcase
    #echo $benchcase >> $file
    for i in `seq 1 1 10`;
    do
      printf "$i:\n" # iteration
      ./benchsuite.sh $benchcase >> $file # results dumped
      sleep 1
    done
  done
done
